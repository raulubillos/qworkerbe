import { Field, InputType, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class LoginWithCredentialOutput {
  @Field()
  accessToken: string;
  @Field()
  apiKey: string;
}

@InputType()
export class LoginWithCredentialInput {
  @Field()
  username: string;

  @Field()
  password: string;
}
