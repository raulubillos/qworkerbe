import { Field, InputType, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class RegisterUserCredentialsOutput {
  @Field()
  userId: string;
  @Field()
  apikey: string;
}

@InputType()
export class RegisterUserCredentialsInput {
  @Field()
  username: string;

  @Field()
  password: string;
}
