import { Args, Mutation, Resolver, Query } from '@nestjs/graphql';
import { AppService } from './app.service';
import { RedisCacheService } from './modules/RedisCacheModule/redisCache.service';
import {
  RegisterUserCredentialsOutput,
  RegisterUserCredentialsInput,
} from './models/registerUserCredentials.model';

import {
  LoginWithCredentialInput,
  LoginWithCredentialOutput,
} from './models/loginWithCredential.model';

@Resolver()
export class AppResolver {
  constructor(
    private appService: AppService,
    private redisCache: RedisCacheService,
  ) {}

  @Query(() => String)
  sayHello(): string {
    return 'Hello World!';
  }

  @Mutation(() => LoginWithCredentialOutput)
  async loginWithCredential(
    @Args('credentials') loginData: LoginWithCredentialInput,
  ) {
    const login = await this.appService.loginWithCredentials(loginData);
    await this.redisCache.set(login.apiKey, login.accessToken);
    return login;
  }

  @Mutation(() => RegisterUserCredentialsOutput)
  async registerUserCredentials(
    @Args('credentials') userCredential: RegisterUserCredentialsInput,
  ) {
    return this.appService.registerUserCredentials(userCredential);
  }
}
