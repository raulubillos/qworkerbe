import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { Cache } from 'cache-manager';

@Injectable()
export class RedisCacheService {
  constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {}

  async get(key) {
    return await this.cacheManager.get(key);
  }

  async set(key, value) {
    await this.cacheManager.set(key, value);
  }
}
