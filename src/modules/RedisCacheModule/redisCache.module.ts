import { Module, CacheModule } from '@nestjs/common';

import * as RedisStore from 'cache-manager-redis-store';
import { RedisCacheService } from './redisCache.service';

@Module({
  imports: [
    CacheModule.register({
      store: RedisStore,
      host: `localhost`,
      port: 6379,
      ttl: 180000,
    }),
  ],
  providers: [RedisCacheService],
  exports: [RedisCacheService],
})
export class RedisCacheModule {}
