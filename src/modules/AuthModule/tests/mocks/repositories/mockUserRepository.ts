import { User } from 'src/modules/AuthModule/model/user.entity';
import { InsertResult } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';

export class MockUserRepository {
  users: User[] = [
    {
      userId: 'test',
      username: 'test',
      password: '$2a$09$WDrlh.OAY0t.LAZNvD42kOBFiPci5EMjI8dj/8RdouKLFDVUXzNcG',
      apikey: 'test',
      createdDate: 122313123,
      lastUpdatedDate: 412332132132,
    },
  ];

  findOne(conditions?: any): Promise<User> {
    return new Promise(resolve => {
      resolve(
        this.users.find(
          user =>
            user.username === conditions.where.username ||
            user.password === conditions.where.password ||
            user.apikey === conditions.where.apikey ||
            user.userId === conditions.where.user_id,
        ),
      );
    });
  }

  insert(entity: QueryDeepPartialEntity<User>): Promise<InsertResult> {
    return new Promise(resolve => {
      this.users.push({
        apikey:
          typeof entity.apikey === 'string' ? entity.apikey : entity.apikey(),
        createdDate:
          typeof entity.createdDate === 'number'
            ? entity.createdDate
            : Number.parseInt(entity.createdDate()),
        lastUpdatedDate:
          typeof entity.lastUpdatedDate === 'number'
            ? entity.lastUpdatedDate
            : Number.parseInt(entity.lastUpdatedDate()),
        username:
          typeof entity.username === 'string'
            ? entity.username
            : entity.username(),
        password:
          typeof entity.password === 'string'
            ? entity.password
            : entity.password(),
        userId: entity.userId
          ? typeof entity.userId === 'string'
            ? entity.userId
            : entity.userId()
          : undefined,
      });
      resolve(new InsertResult());
    });
  }
}
