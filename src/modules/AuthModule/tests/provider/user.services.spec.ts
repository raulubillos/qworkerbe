import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '../../model/user.entity';
import { UserService } from '../../provider/user.services';
import { MockUserRepository } from '../mocks/repositories/mockUserRepository';
import { NonExistentUserException } from '../../exceptions/NonExistentUserException';

describe('UserProvider', () => {
  let userProvider: UserService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        UserService,
        { provide: getRepositoryToken(User), useClass: MockUserRepository },
      ],
    }).compile();
    userProvider = moduleRef.get<UserService>(UserService);
  });

  describe('getUserByApiKey', () => {
    it('test that is returning a value', async () => {
      const user = await userProvider.getUserByApiKey('test');

      expect(user).not.toBeFalsy();
    });
    it('test that is throwing an non existent exception', async () => {
      const userPromise = userProvider.getUserByApiKey('bad');

      await expect(userPromise).rejects.toMatchObject(
        new NonExistentUserException(),
      );
    });
  });

  describe('getUserByUsername', () => {
    it('test that is returning a value', async () => {
      const user = await userProvider.getUserByUsername('test');

      expect(user).not.toBeFalsy();
    });

    it('test that is throwing an non existent exception', async () => {
      const userPromise = userProvider.getUserByUsername('tests');

      await expect(userPromise).rejects.toMatchObject(
        new NonExistentUserException(),
      );
    });
  });

  describe('registerUser', () => {
    it('test that is returning a value', async () => {
      await userProvider.registerUser({
        password: 'test2',
        username: 'test2',
      });
      const user = userProvider.getUserByUsername('test2');

      expect(user).not.toBeFalsy();
    });
  });
});
