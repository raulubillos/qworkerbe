import { UnauthorizedException } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { AuthService } from '../auth.service';
import { User } from '../model/user.entity';
import { UserService } from '../provider/user.services';
import { MockUserRepository } from './mocks/repositories/mockUserRepository';

describe('AuthService', () => {
  let authService: AuthService;
  let userService: UserService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        UserService,
        { provide: getRepositoryToken(User), useClass: MockUserRepository },
        AuthService,
      ],
      imports: [
        PassportModule,
        JwtModule.register({
          secret: 'mockSecretKey',
          signOptions: { expiresIn: '30m' },
        }),
      ],
    }).compile();
    authService = moduleRef.get<AuthService>(AuthService);
    userService = moduleRef.get<UserService>(UserService);
  });

  describe('validateUser', () => {
    it('test that with correct username and password returns a user', async () => {
      const user: User = {
        apikey: 'test',
        createdDate: Date.now(),
        lastUpdatedDate: Date.now(),
        password:
          '$2a$09$WDrlh.OAY0t.LAZNvD42kOBFiPci5EMjI8dj/8RdouKLFDVUXzNcG',
        userId: 'test',
        username: 'test',
      };
      jest
        .spyOn(userService, 'getUserByUsername')
        .mockImplementation(async () => user);
      const validatedUser = await authService.validateUser('test', 'test');
      expect(validatedUser).toBe(user);
    });
    it('test that when user not exist throw unauthorizedexception', async () => {
      jest
        .spyOn(userService, 'getUserByUsername')
        .mockImplementation(async () => undefined);
      const validatedPromise = authService.validateUser('test', 'test');
      await expect(validatedPromise).rejects.toMatchObject(
        new UnauthorizedException(),
      );
    });
    it('test that when we dont have correct password throw unauthorizedexception', async () => {
      const user: User = {
        apikey: 'test',
        createdDate: Date.now(),
        lastUpdatedDate: Date.now(),
        password: 'test1',
        userId: 'test',
        username: 'test',
      };
      jest
        .spyOn(userService, 'getUserByUsername')
        .mockImplementation(async () => user);
      const validatedPromise = authService.validateUser('test', 'test');
      await expect(validatedPromise).rejects.toMatchObject(
        new UnauthorizedException(),
      );
    });
  });

  describe('login', () => {
    it('test that generates a jwt token', async () => {
      const accessToken = await authService.login('test', 'test');
      expect(accessToken.accessToken).not.toBeFalsy();
    });
  });
});
