import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ schema: 'auth', name: 'User' })
export class User {
  @PrimaryGeneratedColumn('uuid')
  userId: string;

  @Column()
  username: string;

  @Column()
  password: string;

  @Column({ type: 'bigint' })
  createdDate: number;

  @Column({ type: 'bigint' })
  lastUpdatedDate: number;

  @Column()
  apikey: string;
}
