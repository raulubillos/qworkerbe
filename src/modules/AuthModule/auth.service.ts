import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from './provider/user.services';
import { JwtService } from '@nestjs/jwt';
import { RegisterUser } from './dto/registerUser.dto';
import { compareSync } from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private userProvider: UserService,
    private jwtService: JwtService,
  ) {}

  async validateUser(username: string, password: string) {
    const user = await this.userProvider.getUserByUsername(username);
    if (!user || !compareSync(password, user.password)) {
      throw new UnauthorizedException();
    }
    return user;
  }

  async login(userId: string, apiKey: string) {
    const payload = { userId, apiKey };

    return {
      accessToken: this.jwtService.sign(payload),
    };
  }

  async register(user: RegisterUser) {
    const id = await this.userProvider.registerUser(user);
    return id;
  }
}
