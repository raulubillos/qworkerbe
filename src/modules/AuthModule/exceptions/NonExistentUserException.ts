import { UnauthorizedException } from '@nestjs/common';

export class NonExistentUserException extends UnauthorizedException {
  constructor() {
    super('NonExistentUser');
  }
}
