import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RegisterUser } from '../dto/registerUser.dto';
import { NonExistentUserException } from '../exceptions/NonExistentUserException';
import { User } from '../model/user.entity';
import { v4 } from 'uuid';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async getUserByApiKey(apiKey: string) {
    const user = await this.userRepository.findOne({
      where: {
        apikey: apiKey,
      },
    });

    if (!user) {
      throw new NonExistentUserException();
    }
    return user;
  }

  async getUserByUsername(username: string) {
    const user = await this.userRepository.findOne({
      where: {
        username: username,
      },
    });

    if (!user) {
      throw new NonExistentUserException();
    }
    return user;
  }

  async registerUser(user: RegisterUser) {
    const apikey = v4();
    const inserted = await this.userRepository.insert({
      apikey: apikey,
      createdDate: new Date().valueOf(),
      lastUpdatedDate: new Date().valueOf(),
      password: user.password,
      username: user.username,
    });

    return {
      userId: inserted.identifiers[0]?.user_id,
      apikey: apikey,
    };
  }
}
