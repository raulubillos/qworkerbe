import { Injectable } from '@nestjs/common';
import { AuthService } from './modules/AuthModule/auth.service';
import { LoginWithCredentialInput } from './models/loginWithCredential.model';
import { RegisterUserCredentialsInput } from './models/registerUserCredentials.model';
import { hash } from 'bcrypt';

@Injectable()
export class AppService {
  constructor(private authService: AuthService) {}

  async loginWithCredentials(loginData: LoginWithCredentialInput) {
    const userData = await this.authService.validateUser(
      loginData.username,
      loginData.password,
    );

    const token = await this.authService.login(
      userData.userId,
      userData.apikey,
    );

    return {
      accessToken: token.accessToken,
      apiKey: userData.apikey,
    };
  }

  async registerUserCredentials(userCredential: RegisterUserCredentialsInput) {
    const userCredentialWithHashedPassword = userCredential;

    userCredentialWithHashedPassword.password = await hash(
      userCredentialWithHashedPassword.password,
      Number.parseInt(process.env.ROUNDS),
    );

    const register = await this.authService.register(
      userCredentialWithHashedPassword,
    );

    return {
      userId: register.userId,
      apikey: register.apikey,
    };
  }
}
