import { Test } from '@nestjs/testing';
import { AppService } from '../app.service';
import { AuthService } from '../modules/AuthModule/auth.service';
import { UserService } from '../modules/AuthModule/provider/user.services';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '../modules/AuthModule/model/user.entity';
import { MockUserRepository } from '../modules/AuthModule/tests/mocks/repositories/mockUserRepository';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { UnauthorizedException } from '@nestjs/common';

describe('AppService', () => {
  let authService: AuthService;
  let appService: AppService;
  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        AppService,
        UserService,
        { provide: getRepositoryToken(User), useClass: MockUserRepository },
        AuthService,
      ],
      imports: [
        PassportModule,
        JwtModule.register({
          secret: 'mockSecretKey',
          signOptions: { expiresIn: '30m' },
        }),
      ],
    }).compile();

    authService = moduleRef.get<AuthService>(AuthService);
    appService = moduleRef.get<AppService>(AppService);
  });

  describe('loginWithCredentials', () => {
    it('test when validated should generate a token', async () => {
      const mockedUser = {
        userId: 'test',
        apikey: 'test',
        username: 'test',
        password: 'test',
        createdDate: Date.now(),
        lastUpdatedDate: Date.now(),
      };
      const mockedToken = {
        accessToken: 'test',
        apiKey: 'test',
      };
      jest.spyOn(authService, 'validateUser').mockImplementation(async () => {
        return mockedUser;
      });
      jest.spyOn(authService, 'login').mockImplementation(async () => {
        return mockedToken;
      });

      const token = await appService.loginWithCredentials({
        password: 'test',
        username: 'test',
      });

      expect(token).toStrictEqual(mockedToken);
    });

    it('test when validate throws UnauthorizedException should throw UnauthorizedException', async () => {
      jest.spyOn(authService, 'validateUser').mockImplementation(async () => {
        throw new UnauthorizedException();
      });

      const loginPromise = appService.loginWithCredentials({
        password: 'test',
        username: 'test',
      });

      expect(loginPromise).rejects.toMatchObject(new UnauthorizedException());
    });
  });
});
